package com.starstruck.adapter

import android.provider.Telephony
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.starstruck.R

const val VIEW_TYPE_MESSAGE_SENT = 1
const val VIEW_TYPE_MESSAGE_RECEIVED = 2

class ChatAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    override fun getItemViewType(position: Int): Int {
        return if (position%2!=0)
            VIEW_TYPE_MESSAGE_SENT
        else
            VIEW_TYPE_MESSAGE_RECEIVED
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            VIEW_TYPE_MESSAGE_RECEIVED -> {
                val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_chat_received_msg, parent,  false)
                ReceivedMessageVH(itemView)
            }
            VIEW_TYPE_MESSAGE_SENT -> {
                val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_chat_sent_msg, parent,  false)
                SentMessageVH(itemView)
            }
            else -> {
                val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_chat_else, parent,  false)
                SentMessageVH(itemView)
            }
        }
    }

    override fun getItemCount(): Int {
        return 9
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder.itemViewType)
        {
            VIEW_TYPE_MESSAGE_RECEIVED -> (holder as ReceivedMessageVH).bind()
            VIEW_TYPE_MESSAGE_SENT -> (holder as SentMessageVH).bind()
        }
    }
}

class SentMessageVH(itemView : View) : RecyclerView.ViewHolder(itemView){
    fun bind()
    {

    }
}

class ReceivedMessageVH(itemView: View) : RecyclerView.ViewHolder(itemView)
{
    private var tvTime : TextView = itemView.findViewById(R.id.tvTimeReceived)
    fun bind()
    {
        if (adapterPosition==0)
            tvTime.visibility = View.VISIBLE
        else
            tvTime.visibility = View.GONE
    }
}