package com.starstruck.adapter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.starstruck.R
import com.starstruck.activity.BaseActivity
import kotlinx.android.synthetic.main.activity_chat.*

class ChatActivity : BaseActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        rvChat.layoutManager = LinearLayoutManager(this)
        rvChat.adapter = ChatAdapter()

        ivBack.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.ivBack -> onBackPressed()
        }
    }
}
