package com.starstruck.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.starstruck.R
import com.starstruck.Utils.OnItemClickListener
import kotlinx.android.synthetic.main.item_card_stack.view.*

class CardStackAdapter(private val listener : OnItemClickListener) : RecyclerView.Adapter<MyVH>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyVH {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_card_stack, parent, false)
        return MyVH(itemView)
    }

    override fun getItemCount(): Int {
        return 10
    }

    override fun onBindViewHolder(holder: MyVH, position: Int) {
        holder.itemView.ivAccept.setOnClickListener {
            listener.onItemClick(it, position, 0)
        }
        holder.itemView.ivReject.setOnClickListener {
            listener.onItemClick(it, position, 0)
        }
        holder.itemView.ivUser.setOnClickListener {
            listener.onItemClick(it, position, 0)
        }
    }
}