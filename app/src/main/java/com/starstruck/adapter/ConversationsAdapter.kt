package com.starstruck.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.starstruck.R
import com.starstruck.Utils.OnItemClickListener

class ConversationsAdapter(private val listener: OnItemClickListener) : RecyclerView.Adapter<MyVH>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyVH {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_conversations, parent, false)
        return MyVH(itemView)
    }

    override fun getItemCount(): Int {
        return 1
    }

    override fun onBindViewHolder(holder: MyVH, position: Int) {
        holder.itemView.setOnClickListener {
            listener.onItemClick(it, position, 0)
        }
    }
}