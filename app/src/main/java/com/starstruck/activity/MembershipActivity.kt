    package com.starstruck.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.starstruck.R
import kotlinx.android.synthetic.main.activity_membership.*

class MembershipActivity : BaseActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_membership)

        window.statusBarColor = ContextCompat.getColor(this, R.color.colorYellow)

        intent.extras?.getInt(MEMBERSHIP)?.let {
            when(it)
            {
                SHOOTING_STAR -> {
                    tvMembershipTitle.text = "shooting star"
                    tvMembershipDesc.text = getString(R.string.shooting_star_signup)
                    ivMembershipDots.setImageResource(R.drawable.img_dot)
                    ivMembership.setImageResource(R.drawable.img_shooting_star)
                    tvMembershipPrice.text = "$3.99/mo"
                }
                SIGN_SEARCH -> {
                    tvMembershipTitle.text = "sign search"
                    tvMembershipDesc.text = getString(R.string.sign_search_desc)
                    ivMembershipDots.setImageResource(R.drawable.img_dot2)
                    ivMembership.setImageResource(R.drawable.img_sign_search)
                    tvMembershipPrice.text = "$5.99/mo"
                }
                ELEMENT_SEARCH -> {
                    tvMembershipTitle.text = "element search"
                    tvMembershipDesc.text = getString(R.string.element_search_desc)
                    ivMembershipDots.setImageResource(R.drawable.img_dot4)
                    ivMembership.setImageResource(R.drawable.img_element)
                    tvMembershipPrice.text = "$4.99/mo"
                }
                MONTHLY -> {
                    tvMembershipTitle.text = "monthly membership"
                    tvMembershipDesc.text = getString(R.string.monthly_membership_desc)
                    ivMembershipDots.setImageResource(R.drawable.img_dot4)
                    ivMembership.setImageResource(R.drawable.ic_logo)
                    tvMembershipPrice.text = "$11.99/mo"
                }
            }
        }

        ivCancel.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.ivCancel -> onBackPressed()
        }
    }
}
