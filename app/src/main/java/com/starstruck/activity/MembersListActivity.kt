package com.starstruck.activity

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.starstruck.adapter.MemberListAdapter
import com.starstruck.R
import com.starstruck.Utils.OnItemClickListener
import kotlinx.android.synthetic.main.activity_members_list.*

class MembersListActivity : BaseActivity(), OnItemClickListener, View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_members_list)

        rvMembers.layoutManager = LinearLayoutManager(this)
        rvMembers.adapter = MemberListAdapter(this)

        ivMessages.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.ivMessages -> launchActivity(MessagesActivity::class.java, null, false)
        }
    }

    override fun onItemClick(view: View, pos: Int, state: Int) {
        launchActivity(ProfileActivity::class.java, null, false)
    }
}
