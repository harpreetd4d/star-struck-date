package com.starstruck.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import com.starstruck.R
import kotlinx.android.synthetic.main.activity_profile_options.*
import kotlinx.android.synthetic.main.lyt_profile_options.*

class ProfileOptionsActivity : BaseActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_options)

        ivSettings.setOnClickListener(this)
        ivEditInfo.setOnClickListener(this)
        lytShootingStar.setOnClickListener(this)
        tvSearchSpecificSign.setOnClickListener(this)
        tvSearchElement.setOnClickListener(this)
        tvMonthlyMembership.setOnClickListener(this)
        ivArrowRight.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.ivSettings -> launchActivity(SettingsActivity::class.java, null, false)
            R.id.ivEditInfo -> launchActivity(EditInfoActivity::class.java, null, false)
            R.id.lytShootingStar -> {
                launchActivity(
                    MembershipActivity::class.java, bundleOf(
                        Pair(
                            MEMBERSHIP,
                            SHOOTING_STAR
                        )
                    ), false
                )
            }
            R.id.tvSearchSpecificSign -> {
                launchActivity(MembershipActivity::class.java, bundleOf(Pair(MEMBERSHIP,
                SIGN_SEARCH)),
                false)
            }
            R.id.tvSearchElement -> {
                launchActivity(MembershipActivity::class.java, bundleOf(Pair(MEMBERSHIP,
                ELEMENT_SEARCH)),
                false)
            }
            R.id.tvMonthlyMembership -> {
                launchActivity(MembershipActivity::class.java, bundleOf(Pair(MEMBERSHIP,
                MONTHLY)),
                false)
            }
            R.id.ivArrowRight -> onBackPressed()
        }
    }
}
