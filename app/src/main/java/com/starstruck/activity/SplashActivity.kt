package com.starstruck.activity

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.view.View
import com.starstruck.R
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.*
import android.content.Intent
import android.provider.MediaStore
import kotlinx.android.synthetic.main.lyt_file_chooser.*
import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import android.view.Window
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.lyt_choose_photo_video.*

const val TAG = "SplashTAG"

class SplashActivity :  BaseActivity(), View.OnClickListener {

    lateinit var dob: Date
    var signIcon: Int? = null
    var isFileChooserShown = false
    private var REQUEST_SELECT_IMAGE = 3
    private var REQUEST_SELECT_VIDEO = 4
    private var REQUEST_CAMERA_PHOTO = 5
    var fileChooserListener: FileChooserListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_splash)

        dob = Date()

//        pushFragment(AnimationFragment())
//
//        groupBack?.setOnClickListener {
//            pushFragment(DateFragment())
//        }

        groupBack.setOnClickListener(this)
        tvCamera.setOnClickListener(this)
        tvUploadVideo.setOnClickListener(this)
        tvFromLibrary.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.tvCamera -> {
                Dialog(this).apply {
                    requestWindowFeature(Window.FEATURE_NO_TITLE)
                    setContentView(R.layout.lyt_choose_photo_video)
                    tvTakePhoto.setOnClickListener {
                        val intent = Intent()
                        intent.action = MediaStore.ACTION_IMAGE_CAPTURE
                        startActivityForResult(intent, REQUEST_CAMERA_PHOTO)
                        dismiss()
                    }
                    tvTakeVideo.setOnClickListener {
                        val intent = Intent()
                        intent.action = MediaStore.ACTION_VIDEO_CAPTURE
                        startActivityForResult(intent, REQUEST_SELECT_VIDEO)
                        dismiss()
                    }
                    show()
                }
            }
            R.id.tvUploadVideo -> {
                val intent = Intent()
                intent.type = "video/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_SELECT_VIDEO)
            }
            R.id.tvFromLibrary -> {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_SELECT_IMAGE)
            }
            R.id.groupBack -> {
                findNavController(R.id.nav_host_fragment).navigate(R.id.action_helloFragment_to_dateFragment)
                hideBackBtn()
            }
        }

        hideFileChooser()

    }

//    fun pushFragment(fragment: Fragment)
//    {
//        if(fragment.javaClass.simpleName == DateFragment().javaClass.simpleName)
//            groupTop.visibility = View.VISIBLE
//
//        if (fragment.javaClass.simpleName == HelloFragment().javaClass.simpleName)
//            groupBack.visibility = View.VISIBLE
//        else
//            groupBack.visibility = View.GONE
//
//        if (fragment.javaClass.simpleName == CareerFragment().javaClass.simpleName) {
//            ivUserSign.visibility = View.VISIBLE
//            signIcon?.let {
//                ivUserSign.setImageResource(it)
//            }
//        }
//
//        val ft = supportFragmentManager.beginTransaction()
//        ft.setCustomAnimations(R.animator.fade_in, R.animator.fade_out)
//        ft.replace(R.id.frameContent, fragment).addToBackStack(null).commit()
//    }

//    fun popBack()
//    {
//        supportFragmentManager.popBackStack()
//    }

    fun showFileChooser(): Boolean {
        lytFileChooser?.visibility = View.VISIBLE
        isFileChooserShown = true
        return true
    }

    fun hideFileChooser(): Boolean {
        lytFileChooser?.visibility = View.GONE
        isFileChooserShown = false
        return false
    }

//    override fun onBackPressed() {
//
//        if (isFileChooserShown)
//            hideFileChooser()
//        else
//            super.onBackPressed()
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_SELECT_IMAGE -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.data?.let {
                        Log.d(TAG, "Photo Uri = $it")
                        val bitmap: Bitmap? = MediaStore.Images.Media.getBitmap(contentResolver, it)
                        bitmap?.let { bm ->
                            fileChooserListener?.onImageSelect(bm)
                        }
                    }
                }
            }
            REQUEST_SELECT_VIDEO -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.data?.let {
                        Log.d(TAG, "Video Uri = $it")
                        fileChooserListener?.onVideoSelect(it)
                    }
                }
            }
            REQUEST_CAMERA_PHOTO -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.extras?.get("data")?.let {
                        Log.d(TAG, "Photo Uri = $it")
                        (it as Bitmap?)?.let { bm ->
                            fileChooserListener?.onImageSelect(bm)
                        }
                    }
                }
            }
        }
    }

    fun openFrontCamera(listener: FileChooserListener)
    {
        this.fileChooserListener = listener
        val intent = Intent()
        intent.action = MediaStore.ACTION_IMAGE_CAPTURE
        intent.putExtra("android.intent.extras.CAMERA_FACING", 1)
        startActivityForResult(intent, REQUEST_CAMERA_PHOTO)
    }


    fun showBackBtn()
    {
        groupBack.visibility = View.VISIBLE
    }

    fun hideBackBtn()
    {
        groupBack.visibility = View.GONE
    }

    fun showTopHeader()
    {
        groupTop.visibility = View.VISIBLE
    }

    fun hideTopHeader()
    {
        groupTop.visibility = View.GONE
    }

    fun showSignIcon()
    {
        ivUserSign.visibility = View.VISIBLE
        graph.getPreferences().getSunSingModel()?.let {
            ivUserSign.setImageResource(it.icon)
        }
    }

    fun hideSignIcon()
    {
        ivUserSign.visibility = View.INVISIBLE
    }


    interface FileChooserListener
    {
        fun onImageSelect(bitmap : Bitmap)
        fun onVideoSelect(uri : Uri)
    }

}
