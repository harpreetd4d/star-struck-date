package com.starstruck.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.google.android.material.snackbar.Snackbar
import com.starstruck.R
import com.starstruck.app.AppComponents
import com.starstruck.app.MyApp

const val SHOOTING_STAR = 1
const val SIGN_SEARCH = 2
const val ELEMENT_SEARCH = 3
const val MONTHLY = 4
const val MEMBERSHIP = "membership"

open class BaseActivity : AppCompatActivity() {

    lateinit var graph : AppComponents

    val signNames = arrayListOf("Aquarius", "Pisces", "Aries" , "Taurus", "Gemini", "Cancer", "Leo", "Virgo",
        "Libra", "Scorpio", "Sagittarius", "Capricorn")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        graph = (application as MyApp).graph
    }

    fun hideKeyboard() {
        val view = currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun showKeyboard(view: View) {
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInputFromWindow(
            view.applicationWindowToken,
            InputMethodManager.SHOW_FORCED, 0)
    }

    fun showSnack(msg: String, rootView: View) {
        Snackbar.make(rootView, msg, Snackbar.LENGTH_SHORT).apply {
            view.setBackgroundColor(ContextCompat.getColor(this@BaseActivity, R.color.colorDarkGrey))
            view.findViewById<TextView>(R.id.snackbar_text).apply {
                typeface = ResourcesCompat.getFont(this@BaseActivity, R.font.montserrat_medium)
            }
            show()
        }
    }

    fun showSnackLong(msg: String, rootView: View) {
        Snackbar.make(rootView, msg, Snackbar.LENGTH_LONG).apply {
            view.setBackgroundColor(ContextCompat.getColor(this@BaseActivity, R.color.colorDarkGrey))
            view.findViewById<TextView>(R.id.snackbar_text).apply {
                typeface = ResourcesCompat.getFont(this@BaseActivity, R.font.montserrat_medium)
            }
            show()
        }
    }

    fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    @SuppressLint("MissingPermission")
    fun isInternetOn(): Boolean {
        val localNetworkInfo = (getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo
        return localNetworkInfo?.isConnected ?: false
    }

    fun isConnectedViaWifi(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val mWifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
        return mWifi.isConnected
    }

    @SuppressLint("HardwareIds")
    fun getDeviceId() : String
    {
        return Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
    }

    internal fun launchActivity(clazz: Class<*>, bundle: Bundle?, clearStack : Boolean)
    {
        val intent = Intent(this, clazz).apply {
            bundle?.let {
                putExtras(bundle)
            }

            if (clearStack)
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }

        startActivity(intent)
    }

    internal fun launchActivityWithoutHistory(clazz: Class<*>, bundle: Bundle?, clearStack : Boolean)
    {
        val intent = Intent(this, clazz).apply {
            bundle?.let {
                putExtras(bundle)
            }

            flags = Intent.FLAG_ACTIVITY_NO_HISTORY

            if (clearStack)
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }

        startActivity(intent)
    }


    internal fun launchActivityWithResult(clazz: Class<*>, requestCode: Int, bundle: Bundle?)
    {
        val intent = Intent(this, clazz).apply {

            bundle?.let {
                putExtras(bundle)
            }

        }

        startActivityForResult(intent, requestCode)
    }

    internal fun launchService(clazz: Class<*>)
    {
        startService(Intent(this, clazz))
    }
}