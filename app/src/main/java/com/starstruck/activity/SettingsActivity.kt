package com.starstruck.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import com.starstruck.R
import kotlinx.android.synthetic.main.activity_settings.*
import kotlinx.android.synthetic.main.lyt_settings.*

class SettingsActivity : BaseActivity(), View.OnClickListener {


    private val listSpinnerGenderOptions = arrayOf("she/her", "he/him", "she/her they/them",
        "he/him they/them", "she/her he/him they/them")

    private val listSpinnerPushNoificationOptions = arrayOf("ON", "OFF")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        val spinnerGenderOptionsAdapter = ArrayAdapter(this, R.layout.item_spinner_settings, R.id
            .tvSpinnerSettingsOptions, listSpinnerGenderOptions)

        val spinnerPushNotificationOptionsAdapter = ArrayAdapter(this, R.layout.item_spinner_settings, R.id
            .tvSpinnerSettingsOptions, listSpinnerPushNoificationOptions)

        spinnerGender.adapter = spinnerGenderOptionsAdapter
        spinnerPushNotification.adapter = spinnerPushNotificationOptionsAdapter
        ivArrowRight.setOnClickListener(this)
        tvCancel.setOnClickListener(this)

    }

    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.ivArrowRight, R.id.tvCancel -> onBackPressed()
        }
    }
}
