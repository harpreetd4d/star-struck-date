package com.starstruck.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import com.github.guilhe.views.SeekBarRangedView
import com.starstruck.R
import kotlinx.android.synthetic.main.activity_edit_info.*
import kotlinx.android.synthetic.main.dialog_shooting_star_signup.*
import kotlinx.android.synthetic.main.lyt_edit_info.*

class EditInfoActivity : BaseActivity(), View.OnClickListener {

    private val listSpinnerGenderOptions = arrayOf("she/her".toUpperCase(), "he/him".toUpperCase(), "she/her they/them".toUpperCase(),
        "he/him they/them".toUpperCase(), "she/her he/him they/them".toUpperCase())

    private val listSpinnerShootingStarsOptions = arrayOf("ON", "OFF")
    private val listSpinnerCompatibleOptions = arrayOf("MOST COMPATIBLE")
    private val listSpinnerCompatible2Options = arrayOf("CASUAL RELATIONSHIP")
    private val listSpinnerHeightOptions = arrayOf("4\'","4\'5\'\'","5\'","5\'5\'\'","6\'")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_info)

        val spinnerGenderOptionsAdapter = ArrayAdapter(this, R.layout.item_spinner_settings, R.id
            .tvSpinnerSettingsOptions, listSpinnerGenderOptions)

        val spinnerShootingStarsAdapter = ArrayAdapter(this, R.layout.item_spinner_settings, R.id
            .tvSpinnerSettingsOptions, listSpinnerShootingStarsOptions)

        val spinnerCompatibleAdapter = ArrayAdapter(this, R.layout.item_spinner_settings, R.id
            .tvSpinnerSettingsOptions, listSpinnerCompatibleOptions)

        val spinnerCompatible2Adapter = ArrayAdapter(this, R.layout.item_spinner_settings, R.id
            .tvSpinnerSettingsOptions, listSpinnerCompatible2Options)

        val spinnerHeightAdapter = ArrayAdapter(this, R.layout.item_spinner_settings, R.id
            .tvSpinnerSettingsOptions, listSpinnerHeightOptions)

        spinnerHeight.adapter = spinnerHeightAdapter
        spinnerLookingForGender.adapter = spinnerGenderOptionsAdapter
        spinnerShootingStars.adapter = spinnerShootingStarsAdapter
        spinnerCompatibility.adapter = spinnerCompatibleAdapter
        spinnerCompatibility2.adapter = spinnerCompatible2Adapter

        sbAgeRange.setOnSeekBarRangedChangeListener(object : SeekBarRangedView.OnSeekBarRangedChangeListener{
            override fun onChanged(view: SeekBarRangedView?, minValue: Float, maxValue: Float) {

            }

            override fun onChanging(view: SeekBarRangedView?, minValue: Float, maxValue: Float) {
               tvAgeRange.text = "${minValue.toInt()} - ${maxValue.toInt()}"
            }

        })

        ivShootingStar.setOnClickListener(this)
        rootLayout.setOnClickListener(this)
        innerRootLayout.setOnClickListener(this)
        rootLayoutDialogShootingStar.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.ivShootingStar -> rootLayoutDialogShootingStar.visibility = View.VISIBLE
            R.id.rootLayout, R.id.innerRootLayout -> hideKeyboard()
            R.id.rootLayoutDialogShootingStar ->  rootLayoutDialogShootingStar.visibility = View.INVISIBLE
            R.id.tvCancel, R.id.ivBack -> onBackPressed()
        }
    }

    override fun onBackPressed() {
        if(rootLayoutDialogShootingStar.visibility==View.VISIBLE)
            rootLayoutDialogShootingStar.visibility = View.INVISIBLE
        else
            super.onBackPressed()


    }
}
