package com.starstruck.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.starstruck.R
import com.starstruck.adapter.MostCompatibleAdapter
import kotlinx.android.synthetic.main.activity_likes_you.*

class LikesYouActivity : BaseActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_likes_you)

        rvMostCompatible.layoutManager = LinearLayoutManager(this)
        rvTakeChance.layoutManager = LinearLayoutManager(this)
        rvMostCompatible.adapter = MostCompatibleAdapter()
        rvTakeChance.adapter = MostCompatibleAdapter()

        ivSwipeRightStars.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.ivSwipeRightStars-> onBackPressed()
        }
    }
}
