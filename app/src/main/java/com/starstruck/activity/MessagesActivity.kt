package com.starstruck.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.starstruck.R
import com.starstruck.Utils.OnItemClickListener
import com.starstruck.adapter.ChatActivity
import com.starstruck.adapter.ConversationsAdapter
import com.starstruck.adapter.YouStruckWithAdapter
import kotlinx.android.synthetic.main.activity_messages.*

class MessagesActivity : BaseActivity(), View.OnClickListener, OnItemClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_messages)

        rvYouStruckWith.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        rvYouStruckWith.adapter = YouStruckWithAdapter()
        rvConversations.layoutManager = LinearLayoutManager(this)
        rvConversations.adapter = ConversationsAdapter(this)

        ivLeftSwipeStars.setOnClickListener(this)
        ivRightSwipeStars.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.ivLeftSwipeStars -> launchActivity(LikesYouActivity::class.java, null , false)
            R.id.ivRightSwipeStars -> launchActivity(ArchivedChatsActivity::class.java, null, false)
        }
    }

    override fun onItemClick(view: View, pos: Int, state: Int) {
        launchActivity(ChatActivity::class.java, null, false)
    }
}
