package com.starstruck.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.starstruck.R
import com.starstruck.adapter.ArchivedChatsAdapter
import kotlinx.android.synthetic.main.activity_archived_chats.*

class ArchivedChatsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_archived_chats)

        rvArchivedChats.layoutManager = LinearLayoutManager(this)
        rvArchivedChats.adapter = ArchivedChatsAdapter()
    }
}
