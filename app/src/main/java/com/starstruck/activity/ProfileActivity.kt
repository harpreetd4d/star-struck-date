package com.starstruck.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.starstruck.R
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        ivChat.setOnClickListener {
            launchActivity(MessagesActivity::class.java, null, false)
        }
    }
}
