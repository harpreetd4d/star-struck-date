package com.starstruck.Utils

import android.view.View

interface OnItemClickListener {
    fun onItemClick(view: View, pos : Int, state : Int)
}