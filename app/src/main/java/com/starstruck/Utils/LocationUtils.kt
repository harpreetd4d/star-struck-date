package com.starstruck.Utils

import android.Manifest
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task

const val REQUEST_CHECK_SETTINGS = 201
const val REQUEST_CHECK_PERMISSIONS = 301

class LocationUtils(private val fragment : Fragment) {

    constructor(fragment : Fragment, listener : LocationUtilsListener) : this(fragment)
    {
        this.listener = listener
    }

    var listener : LocationUtilsListener? = null
    var locationRequest : LocationRequest? = null
    val builder = LocationSettingsRequest.Builder()
//    val client: SettingsClient = LocationServices.getSettingsClient(activity)
//    val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
    var client : SettingsClient? = null
    var task : Task<LocationSettingsResponse>? = null

    fun setup()
    {
        client = LocationServices.getSettingsClient(fragment.context!!)
        task = client?.checkLocationSettings(builder.build())

        createLocationRequest()
        addLocationRequest()

        task?.addOnSuccessListener { locationSettingsResponse ->
            // All location settings are satisfied. The client can initialize
            // location requests here.
            // ...
            listener?.onAllThingsDone()
        }

        task?.addOnFailureListener { exception ->
            if (exception is ResolvableApiException){
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
//                    exception.startResolutionForResult(fragment.activity,
//                        REQUEST_CHECK_SETTINGS)
                    fragment.startIntentSenderForResult(exception.resolution.intentSender,
                        REQUEST_CHECK_SETTINGS,
                        null, 0, 0, 0, null)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }


    fun createLocationRequest() {
        locationRequest = LocationRequest.create()?.apply {
            interval = 10000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
    }

    fun addLocationRequest()
    {
        locationRequest?.let {
            builder.addLocationRequest(it)
        }

        client = LocationServices.getSettingsClient(fragment.context!!)
        task = client?.checkLocationSettings(builder.build())
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    {
        when(requestCode)
        {
            REQUEST_CHECK_SETTINGS ->
            {
                listener?.onAllThingsDone()
            }
        }
    }

    fun onPermissionsCallback(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)
    {
        if (requestCode== REQUEST_CHECK_PERMISSIONS)
        {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                setup()
            }
        }
    }

    fun checkPermissions()
    {
        if (ContextCompat.checkSelfPermission(fragment.context!!, Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
            askPermissions()
        }
        else
        {
            setup()
        }
    }

    fun askPermissions()
    {
        fragment.requestPermissions(arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
            REQUEST_CHECK_PERMISSIONS
        )
    }

    interface LocationUtilsListener
    {
        fun onAllThingsDone()
    }
}