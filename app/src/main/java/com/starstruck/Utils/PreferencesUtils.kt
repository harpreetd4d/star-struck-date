package com.starstruck.Utils

import android.content.SharedPreferences
import androidx.core.content.edit
import com.google.gson.Gson
import com.starstruck.SunSignModel
import javax.inject.Inject

const val SUN_SIGN = "sun_sign"

class PreferencesUtils @Inject constructor(private var prefrences : SharedPreferences) {

    fun saveSunSignModel(model : SunSignModel)
    {
        val gson = Gson()
        val json = gson.toJson(model)
        prefrences.edit {
            putString(SUN_SIGN, json)
        }
    }

    fun getSunSingModel() : SunSignModel?
    {
        val gson = Gson()
        val json = prefrences.getString(SUN_SIGN, "")
        if (json!=null && json.isNotEmpty())
        {
            val model = gson.fromJson<SunSignModel>(json, SunSignModel::class.java)
            return model
        }
        else
            return null

    }
}