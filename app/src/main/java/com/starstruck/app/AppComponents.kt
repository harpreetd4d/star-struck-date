package com.starstruck.app

import android.app.Application
import com.starstruck.Utils.PreferencesUtils
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponents {
    fun inject(application: Application)
    fun getApplication() : Application
    fun getPreferences() : PreferencesUtils
}