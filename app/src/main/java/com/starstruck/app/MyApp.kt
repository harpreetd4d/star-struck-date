package com.starstruck.app

import android.app.Application
import com.starstruck.Utils.TypefaceUtil

class MyApp : Application() {

    lateinit var graph : AppComponents
    override fun onCreate() {
        super.onCreate()

        TypefaceUtil.overrideFont(this, "SERIF", "fonts/montserrat_regular.otf")

        initDagger()
    }

    fun initDagger()
    {
        graph = DaggerAppComponents.builder()
            .appModule(AppModule(this))
            .build()

        graph.inject(this)
    }

}