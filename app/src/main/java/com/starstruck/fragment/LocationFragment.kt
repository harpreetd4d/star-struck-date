package com.starstruck.fragment


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.starstruck.Utils.LocationUtils

import com.starstruck.R
import com.starstruck.activity.MainActivity
import com.starstruck.activity.MembersListActivity
import com.starstruck.activity.ProfileActivity
import com.starstruck.activity.SplashActivity
import kotlinx.android.synthetic.main.fragment_location.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class LocationFragment : Fragment(), View.OnClickListener {

    private var splashActivity : SplashActivity? = null
    private var locationUtils : LocationUtils? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_location, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        splashActivity = activity as SplashActivity
        locationUtils = LocationUtils(this)
        tvSettings.setOnClickListener(this)
        tvNext.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.tvSettings -> locationUtils?.checkPermissions()
            R.id.tvNext -> splashActivity?.launchActivity(MainActivity::class.java, null , true)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("LocationIssue", "onActivityResult - requestCode = $requestCode")
        locationUtils?.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.d("LocationIssue", "onRequestPermissionsResult - requestCode = $requestCode")
        locationUtils?.onPermissionsCallback(requestCode, permissions, grantResults)
    }

}
