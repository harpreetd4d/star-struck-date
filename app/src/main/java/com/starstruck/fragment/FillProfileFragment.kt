package com.starstruck.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.navigation.fragment.findNavController

import com.starstruck.R
import com.starstruck.activity.SplashActivity
import kotlinx.android.synthetic.main.fragment_fill_profile.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FillProfileFragment : Fragment(), View.OnClickListener {

    private var splashActivity : SplashActivity? = null
    private val listSpinnerOptions = arrayOf("she/her", "he/him", "she/her they/them",
        "he/him they/them", "she/her he/him they/them")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fill_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        splashActivity = activity as SplashActivity

        splashActivity?.graph?.getPreferences()?.getSunSingModel()?.let {
            tvAlmostDone.text = "hey ${it.name}!\nyou're almost done"
        }

        val spinnerOptionsAdapter = ArrayAdapter(context!!, R.layout.item_spinner_options, R.id
            .tvSpinnerOptions, listSpinnerOptions)

        spinnerPronouns.adapter = spinnerOptionsAdapter
        spinnerLooking.adapter = spinnerOptionsAdapter

        rootLayout.setOnClickListener(this)
        tvNext.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.rootLayout -> splashActivity?.hideKeyboard()
            R.id.tvNext -> findNavController().navigate(R.id.action_fillProfileFragment_to_addPicsVideosFragment)
        }
    }


}
