package com.starstruck.fragment


import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.ContextCompat

import com.starstruck.R
import com.starstruck.activity.SplashActivity
import kotlinx.android.synthetic.main.fragment_add_pics_videos.*
import androidx.navigation.fragment.findNavController
import com.starstruck.Utils.FileUtils


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class AddPicsVideosFragment : Fragment(), View.OnClickListener, SplashActivity.FileChooserListener {

    private var splashActivity : SplashActivity? = null
    private val PERMISSIONS_REQUEST_READ_STORAGE = 1
    private val PERMISSIONS_REQUEST_CAMERA = 2
    private var imageView : ImageView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_pics_videos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        splashActivity = activity as SplashActivity
        splashActivity?.fileChooserListener = this

        viewRules?.setOnClickListener(this)
        cv1?.setOnClickListener(this)
        cv2?.setOnClickListener(this)
        cv3?.setOnClickListener(this)
        cv4?.setOnClickListener(this)
        cv5?.setOnClickListener(this)
        cv6?.setOnClickListener(this)
        rootLayout?.setOnClickListener(this)
        tvNext?.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.cv1 -> {
                imageView = iv1
                askPermissions()
            }
            R.id.cv2 -> {
                imageView = iv2
                askPermissions()
            }
            R.id.cv3 -> {
                imageView = iv3
                askPermissions()
            }
            R.id.cv4 -> {
                imageView = iv4
                askPermissions()
            }
            R.id.cv5 -> {
                imageView = iv5
                askPermissions()
            }
            R.id.cv6 -> {
                imageView = iv6
                askPermissions()
            }
            R.id.rootLayout ->
            {
                splashActivity?.hideFileChooser()!!
            }
            R.id.viewRules ->
            {
                if (splashActivity?.isFileChooserShown!!)
                    splashActivity?.hideFileChooser()!!
                else
                    findNavController().navigate(R.id.action_addPicsVideosFragment_to_photoRulesFragment)
            }
            R.id.tvNext ->
            {
                if(validate())
                    findNavController().navigate(R.id.action_addPicsVideosFragment_to_verifyYourselfFragment)
                else
                    splashActivity?.showToast("Minimum 1 photo/video required")
            }
        }
    }

    fun validate() : Boolean
    {
        var isAdded = false
        iv1?.tag?.let {
            if (it==1)
                isAdded = true
        }
        iv2?.tag?.let {
            if (it==1)
                isAdded = true
        }
        iv3?.tag?.let {
            if (it==1)
                isAdded = true
        }
        iv4?.tag?.let {
            if (it==1)
                isAdded = true
        }
        iv5?.tag?.let {
            if (it==1)
                isAdded = true
        }
        iv6?.tag?.let {
            if (it==1)
                isAdded = true
        }
        return isAdded
    }

    override fun onImageSelect(bitmap: Bitmap) {
        imageView?.setImageBitmap(bitmap)
        imageView?.tag = 1
    }

    override fun onVideoSelect(uri: Uri) {
        val videoPath = FileUtils.getPath(context!!, uri)

        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(context!!, uri)
        val time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
        val timeInMilisec = time.toLong()
        val timeSec = timeInMilisec/1000
        if (timeSec>10) {
            Toast.makeText(context!!, "Video can be a maximum of 10 seconds", Toast.LENGTH_SHORT).show()
            return
        }


        createVideoThumbnail(videoPath)?.let {
            imageView?.setImageBitmap(it)
            imageView?.tag = 1
        }
    }

    private fun createVideoThumbnail(videoPath : String) : Bitmap?
    {
        return ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MICRO_KIND)
    }

    private fun askPermissions()
    {
        if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                PERMISSIONS_REQUEST_READ_STORAGE)
        }
        else
        {
            if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
               requestPermissions(arrayOf(Manifest.permission.CAMERA),
                    PERMISSIONS_REQUEST_CAMERA)
            }
            else
                splashActivity?.showFileChooser()!!
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when(requestCode)
        {
            PERMISSIONS_REQUEST_READ_STORAGE ->
            {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    requestPermissions(arrayOf(Manifest.permission.CAMERA),
                        PERMISSIONS_REQUEST_CAMERA)
                }
                else
                {
                    Toast.makeText(context!!, "Allow permission to move further", Toast.LENGTH_SHORT).show()
                    requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                        PERMISSIONS_REQUEST_READ_STORAGE)
                }
            }
            PERMISSIONS_REQUEST_CAMERA ->
            {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                   splashActivity?.showFileChooser()!!
                }
                else
                {
                    Toast.makeText(context!!, "Allow permission to move further", Toast.LENGTH_SHORT).show()
                    requestPermissions(arrayOf(Manifest.permission.CAMERA),
                        PERMISSIONS_REQUEST_CAMERA)
                }
            }

        }
    }

}
