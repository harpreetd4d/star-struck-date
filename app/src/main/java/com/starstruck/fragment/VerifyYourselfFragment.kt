package com.starstruck.fragment


import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController

import com.starstruck.R
import com.starstruck.activity.SplashActivity
import kotlinx.android.synthetic.main.fragment_verify_yourself.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


/**
 * A simple [Fragment] subclass.
 *
 */
class VerifyYourselfFragment : Fragment() {

    private var splashActivity : SplashActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_verify_yourself, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        splashActivity = activity as SplashActivity

        tvCamera?.setOnClickListener {
            splashActivity?.openFrontCamera(object : SplashActivity.FileChooserListener{
                override fun onImageSelect(bitmap: Bitmap) {
                    ivUserSelfie.setImageBitmap(bitmap)
                    splashActivity?.showToast("Please wait...")
                    Handler().postDelayed({
                        findNavController().navigate(R.id.action_verifyYourselfFragment_to_vowFragment)
                    }, 1000)
                }

                override fun onVideoSelect(uri: Uri) {

                }
            })
//            findNavController().navigate(R.id.action_verifyYourselfFragment_to_locationFragment)
        }
    }

}
