package com.starstruck.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.starstruck.activity.SplashActivity
import kotlinx.android.synthetic.main.fragment_date.*
import android.app.DatePickerDialog
import android.widget.DatePicker
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.starstruck.R
import com.starstruck.SunSignModel
import java.sql.Time
import java.time.Year
import java.util.*
import kotlin.collections.ArrayList
import java.util.Arrays.asList





private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class DateFragment : Fragment(), View.OnClickListener {

    private var splashActivity : SplashActivity? = null

    private var dateOfBirth = Calendar.getInstance()

    val signModel = SunSignModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_date, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        splashActivity = activity as SplashActivity

        splashActivity?.showTopHeader()
        dateOfBirth.time = splashActivity?.dob!!

        tvDateMonth?.text = "${getMonth(dateOfBirth[Calendar.MONTH])} ${dateOfBirth[Calendar.DAY_OF_MONTH]}"
        tvYear?.text = dateOfBirth[Calendar.YEAR].toString()

        getZodiacSign()

        tvNext.setOnClickListener(this)
        tvDateMonth.setOnClickListener(this)
        tvYear.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.tvNext -> {
                splashActivity?.dob = dateOfBirth.time
                findNavController().navigate(R.id.action_dateFragment_to_helloFragment)
            }
            R.id.tvDateMonth, R.id.tvYear -> showDateMonthPicker()
        }
    }

    private fun showDateMonthPicker() {
        val datePickerDialog = DatePickerDialog(context!!,
            DatePickerDialog.OnDateSetListener { datePicker, year, month, dateOfMonth ->
                tvDateMonth.text = "${getMonth(month)} $dateOfMonth"
                tvYear.text = year.toString()
                val cal = Calendar.getInstance()
                cal[Calendar.YEAR] = year
                cal[Calendar.MONTH] = month
                cal[Calendar.DAY_OF_MONTH] = dateOfMonth
                dateOfBirth.time = cal.time
                getZodiacSign()
            }, dateOfBirth[Calendar.YEAR], dateOfBirth[Calendar.MONTH], dateOfBirth[Calendar.DAY_OF_MONTH])
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis().minus(86400000)
        datePickerDialog.show()
    }

    private fun getZodiacSign()
    {
        val checkDate = Calendar.getInstance()
        checkDate[Calendar.YEAR] = dateOfBirth[Calendar.YEAR]
        val checkDate2 = Calendar.getInstance()
        checkDate2[Calendar.YEAR] = dateOfBirth[Calendar.YEAR]
        checkDate[Calendar.MONTH] = 0
        checkDate[Calendar.DAY_OF_MONTH] = 20-1
        checkDate2[Calendar.MONTH] = 1
        checkDate2[Calendar.DAY_OF_MONTH] = 18+1
        val tags = splashActivity?.signNames
        if (dateOfBirth.after(checkDate) && dateOfBirth.before(checkDate2))
        {
            signModel.id = 1
            signModel.icon = R.drawable.ic_aquarius
            signModel.name = tags!![0]
        }
        else
        {
            checkDate[Calendar.MONTH] = 1
            checkDate[Calendar.DAY_OF_MONTH] = 19-1
            checkDate2[Calendar.MONTH] = 2
            checkDate2[Calendar.DAY_OF_MONTH] = 20+1
            if (dateOfBirth.after(checkDate) && dateOfBirth.before(checkDate2))
            {
                signModel.id = 2
                signModel.icon = R.drawable.ic_pisces
                signModel.name = tags!![1]
            }
            else
            {
                checkDate[Calendar.MONTH] = 2
                checkDate[Calendar.DAY_OF_MONTH] = 21-1
                checkDate2[Calendar.MONTH] = 3
                checkDate2[Calendar.DAY_OF_MONTH] = 19+1
                if (dateOfBirth.after(checkDate) && dateOfBirth.before(checkDate2))
                {
                    signModel.id = 3
                    signModel.icon = R.drawable.ic_aries
                    signModel.name = tags!![2]
                }
                else
                {
                    checkDate[Calendar.MONTH] = 3
                    checkDate[Calendar.DAY_OF_MONTH] = 20-1
                    checkDate2[Calendar.MONTH] = 4
                    checkDate2[Calendar.DAY_OF_MONTH] = 20+1
                    if (dateOfBirth.after(checkDate) && dateOfBirth.before(checkDate2))
                    {
                        signModel.id = 4
                        signModel.icon = R.drawable.ic_taurus
                        signModel.name = tags!![3]
                    }
                    else
                    {
                        checkDate[Calendar.MONTH] = 4
                        checkDate[Calendar.DAY_OF_MONTH] = 21-1
                        checkDate2[Calendar.MONTH] = 5
                        checkDate2[Calendar.DAY_OF_MONTH] = 20+1
                        if (dateOfBirth.after(checkDate) && dateOfBirth.before(checkDate2))
                        {
                            signModel.id = 5
                            signModel.icon = R.drawable.ic_gemini
                            signModel.name = tags!![4]
                        }
                        else
                        {
                            checkDate[Calendar.MONTH] = 5
                            checkDate[Calendar.DAY_OF_MONTH] = 21-1
                            checkDate2[Calendar.MONTH] = 6
                            checkDate2[Calendar.DAY_OF_MONTH] = 22+1
                            if (dateOfBirth.after(checkDate) && dateOfBirth.before(checkDate2))
                            {
                                signModel.id = 6
                                signModel.icon = R.drawable.ic_cancer
                                signModel.name = tags!![5]
                            }
                            else
                            {
                                checkDate[Calendar.MONTH] = 6
                                checkDate[Calendar.DAY_OF_MONTH] = 23-1
                                checkDate2[Calendar.MONTH] = 7
                                checkDate2[Calendar.DAY_OF_MONTH] = 22+1
                                if (dateOfBirth.after(checkDate) && dateOfBirth.before(checkDate2))
                                {
                                    signModel.id = 7
                                    signModel.icon = R.drawable.ic_leo
                                    signModel.name = tags!![6]
                                }
                                else
                                {
                                    checkDate[Calendar.MONTH] = 7
                                    checkDate[Calendar.DAY_OF_MONTH] = 23-1
                                    checkDate2[Calendar.MONTH] = 8
                                    checkDate2[Calendar.DAY_OF_MONTH] = 22+1
                                    if (dateOfBirth.after(checkDate) && dateOfBirth.before(checkDate2))
                                    {
                                        signModel.id = 8
                                        signModel.icon = R.drawable.ic_virgo
                                        signModel.name = tags!![7]
                                    }
                                    else
                                    {
                                        checkDate[Calendar.MONTH] = 8
                                        checkDate[Calendar.DAY_OF_MONTH] = 23-1
                                        checkDate2[Calendar.MONTH] = 9
                                        checkDate2[Calendar.DAY_OF_MONTH] = 22+1
                                        if (dateOfBirth.after(checkDate) && dateOfBirth.before(checkDate2))
                                        {
                                            signModel.id = 9
                                            signModel.icon = R.drawable.ic_libra
                                            signModel.name = tags!![8]
                                        }
                                        else
                                        {
                                            checkDate[Calendar.MONTH] = 9
                                            checkDate[Calendar.DAY_OF_MONTH] = 23-1
                                            checkDate2[Calendar.MONTH] = 10
                                            checkDate2[Calendar.DAY_OF_MONTH] = 21+1
                                            if (dateOfBirth.after(checkDate) && dateOfBirth.before(checkDate2))
                                            {
                                                signModel.id = 10
                                                signModel.icon = R.drawable.ic_scorpio
                                                signModel.name = tags!![9]
                                            }
                                            else
                                            {
                                                checkDate[Calendar.MONTH] = 10
                                                checkDate[Calendar.DAY_OF_MONTH] = 22-1
                                                checkDate2[Calendar.MONTH] = 11
                                                checkDate2[Calendar.DAY_OF_MONTH] = 21+1
                                                if (dateOfBirth.after(checkDate) && dateOfBirth.before(checkDate2))
                                                {
                                                    signModel.id = 11
                                                    signModel.icon = R.drawable.ic_sagittarius
                                                    signModel.name = tags!![10]
                                                }
                                                else
                                                {
                                                    checkDate[Calendar.MONTH] = 11
                                                    checkDate[Calendar.DAY_OF_MONTH] = 22-1
                                                    checkDate2[Calendar.MONTH] = 0
                                                    checkDate2[Calendar.DAY_OF_MONTH] = 19+1
                                                    if (dateOfBirth.after(checkDate) && dateOfBirth.before(checkDate2))
                                                    {
                                                        signModel.id = 12
                                                        signModel.icon = R.drawable.ic_capricorn
                                                        signModel.name = tags!![11]
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        splashActivity?.graph?.getPreferences()?.saveSunSignModel(signModel)
    }

    private fun getMonth(month : Int) : String
    {
        return when(month)
        {
            0 -> "January"
            1 -> "February"
            2 -> "March"
            3 -> "April"
            4 -> "May"
            5 -> "June"
            6 -> "July"
            7 -> "August"
            8 -> "September"
            9 -> "October"
            10 -> "November"
            11 -> "December"
            else -> ""
        }
    }

}
