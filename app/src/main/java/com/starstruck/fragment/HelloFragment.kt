package com.starstruck.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController

import com.starstruck.R
import com.starstruck.activity.SplashActivity
import kotlinx.android.synthetic.main.fragment_hello.*

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class HelloFragment : Fragment() {

    private var splashActivity : SplashActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_hello, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        splashActivity = activity as SplashActivity

        splashActivity?.showBackBtn()

        splashActivity?.graph?.getPreferences()?.getSunSingModel()?.let {
            ivSign.setImageResource(it.icon)
            tvSign.text = it.name
        }


        tvNext.setOnClickListener {
            findNavController().navigate(R.id.action_helloFragment_to_careerFragment)
            splashActivity?.hideBackBtn()
        }

    }


}
