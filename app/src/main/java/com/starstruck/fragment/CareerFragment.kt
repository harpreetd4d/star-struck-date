package com.starstruck.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController

import com.starstruck.R
import com.starstruck.activity.SplashActivity
import kotlinx.android.synthetic.main.fragment_career.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class CareerFragment : Fragment(), View.OnClickListener {
    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.tvAgree ->
            {
                tvDisagree.background = ContextCompat.getDrawable(context!!, R.drawable
                    .shape_outline_yellow_solid_white_round_sides)
                tvAgree.background = ContextCompat.getDrawable(context!!, R.drawable.shape_outline_yellow_solid_light_yellow_round_sides)
            }
            R.id.tvDisagree ->
            {
                tvAgree.background = ContextCompat.getDrawable(context!!, R.drawable
                    .shape_outline_yellow_solid_white_round_sides)
                tvDisagree.background = ContextCompat.getDrawable(context!!, R.drawable
                    .shape_outline_yellow_solid_light_yellow_round_sides)
            }
        }

    }

    private var splashActivity : SplashActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_career, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        splashActivity = activity as SplashActivity

        splashActivity?.showSignIcon()

        tvAgree.setOnClickListener {
            findNavController().navigate(R.id.action_careerFragment_to_quizResultFragment)
        }

        tvAgree.setOnTouchListener { view, motionEvent ->
            tvDisagree.background = ContextCompat.getDrawable(context!!, R.drawable
                .shape_outline_yellow_solid_white_round_sides)
            tvAgree.background = ContextCompat.getDrawable(context!!, R.drawable.shape_outline_yellow_solid_light_yellow_round_sides)
            false
        }

        tvDisagree.setOnTouchListener { view, motionEvent ->
            tvAgree.background = ContextCompat.getDrawable(context!!, R.drawable
                .shape_outline_yellow_solid_white_round_sides)
            tvDisagree.background = ContextCompat.getDrawable(context!!, R.drawable
                .shape_outline_yellow_solid_light_yellow_round_sides)
            false
        }
    }

}
