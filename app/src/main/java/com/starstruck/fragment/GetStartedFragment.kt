package com.starstruck.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController

import com.starstruck.R
import com.starstruck.activity.SplashActivity
import kotlinx.android.synthetic.main.fragment_get_started.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class LetsPlayFragment : Fragment() {

    private var splashActivity : SplashActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_get_started, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        splashActivity = activity as SplashActivity

        btnLetsPlay?.setOnClickListener {
            findNavController().navigate(R.id.action_letsPlayFragment_to_dateFragment)
        }

    }

}
